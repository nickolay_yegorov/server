class Post < ActiveRecord::Base
  has_one :like

  after_create :create_like

  private

  def create_like
    Like.create!(post: self)
  end
end
