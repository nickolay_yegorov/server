class Like < ActiveRecord::Base
  belongs_to :post

  def incr
    update(count: count + 1)
  end
end
