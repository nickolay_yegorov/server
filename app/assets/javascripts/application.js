// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready(function(){
  $(".main_page-send_button").on('click', function(){
    var field=$('#main_page-name_area');
    var name = field.val();
    if (name === "") {
      field.css('outline', 'red solid 1px');
    }

    field = $('#main_page-msg_area');
    var message = field.val();
    if (message === "") {
      field.css('outline', 'red solid 1px');
    }

    if(message.length > 0 && name.length > 0){
      var post_data = {
        "post[nickname]": name,
        "post[text]": message
      };
      $.post('/posts', post_data, function(data){
        $('#msgs-container').prepend(data);
      });
    }

    $(document).on('click', '.like_button', function(e){
        var el = $(e.target);
        var id = el.attr('data-post-id');

        $.post('/like/' + id, function(data){
          $(el).text('Нравится: ' + data['count']);
        });
        
    });

    $(document).on('click', '.delete_button', function(e){
        var id = $(e.target).attr('data-post-id');
        $.post('/destroy/' + id);
        $('#post-' + id).hide();
    });

  });
});